package models;

/**
 * Created with IntelliJ IDEA.
 * User: utmrtt
 * Date: 9/23/13
 * Time: 1:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class HeatConverter {
    public static final double constRate = 4.76;

    private double volume;
    private double rate;
    private double currentTemp;

    public double calculateCurrentTemp(){
        return volume / rate *constRate;
    }

    public double getCurrentTemp() {

        return currentTemp;

    }

    public void setCurrentTemp(double currentTemp) {
        this.currentTemp = currentTemp;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

}
