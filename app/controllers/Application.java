package controllers;

import models.HeatConverter;
import play.data.Form;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

//import static play.data.Form.form;

public class Application extends Controller {
    //static Form<HeatConverter> heatForm = form(HeatConverter.class);

        public static Result index() {
        HeatConverter heatConverter = new HeatConverter();
                        //heatConverter.setVolume(1000.0);
        return ok(index.render());
    }

    @BodyParser.Of(BodyParser.TolerantJson.class)
    public static Result submit() {
        //System.out.println("values are come");
        double currentTemp;
        final HeatConverter heatConverter = Json.fromJson(request().body().asJson(), HeatConverter.class);

        currentTemp = heatConverter.calculateCurrentTemp();


        heatConverter.setCurrentTemp(currentTemp);


        return ok(Json.toJson(heatConverter));
    }
}
