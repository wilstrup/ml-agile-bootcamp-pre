package models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TodoListTest {
    @Test
    public void todoListCreation() {
        TodoList todoList = new TodoList();

        assertEquals(1, todoList.all().size());
        Todo t = todoList.all().get(0);
        assertEquals("The first todo label should be the example", "Example Todo", t.getLabel());
        assertEquals("The first todo should have id 1", (Integer)1, t.getId());
    }

    @Test
    public void todoListAddition() {
        TodoList todoList = new TodoList();

        assertEquals(1, todoList.all().size());
        Todo todo = new Todo("New Example");
        todoList.create(todo);
        int key = todo.getId();
        assertEquals("The id should be the next in line", 2, key);
        assertEquals("The todo list should have one more element", 2, todoList.all().size());
        assertTrue("The todo list should contain the todo item", todoList.all().contains(todo));
    }

    @Test
    public void todoListGet() {
        TodoList todoList = new TodoList();
        Todo todo = todoList.get(1);

        assertEquals("The Label matches the expected todo", "Example Todo", todo.getLabel());
    }

    @Test
    public void todoListDelete() {
        TodoList todoList = new TodoList();
        assertEquals(1, todoList.all().size());

        todoList.delete(1);
        assertEquals(0, todoList.all().size());
    }
}
