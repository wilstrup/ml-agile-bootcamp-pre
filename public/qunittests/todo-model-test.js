(function () {
    "use strict";
    module("Todo Model Tests", {
        setup: function setup() {
            var self = this;
            self.updateTodoCallCounter = 0;
            self.server = {
                updateTodo: function updateTodo() {
                    self.updateTodoCallCounter++;
                }
            };
            self.data = [
                {
                    label: "Test Todo",
                    completed: false,
                    id: 1
                },
                {
                    label: "Another Todo",
                    completed: true,
                    id: 2
                }
            ];
        }
    });

    test("Todo Creation", function todoCreation() {
        var todo = new TodoModel(this.data[0], this.server);

        strictEqual(this.data[0].label, todo.label());
        strictEqual(this.data[0].completed, todo.completed());
        strictEqual(this.data[0].id, todo.id());
    });

    test("Todo Update Completion", function todoUpdate() {
        var todo = new TodoModel(this.data[0], this.server);

        strictEqual(this.updateTodoCallCounter, 0, "Make sure that the server has not been called");

        todo.completed(true);

        strictEqual(this.updateTodoCallCounter, 1, "After updating the completion status the server should have been called");
    });

    test("Todo List Creation", function todoListCreation() {
        var todos = ko.mapping.fromJS(this.data, new TodoModelMapping(this.server));
        strictEqual(todos().length, 2, "Assert that the two data objects have been converted");
    });

    test("Update Todo List", function todoListUpdate() {
        var todos = ko.mapping.fromJS(this.data, new TodoModelMapping(this.server));
        var oldFirstTodo = todos()[0];
        var oldSecondTodo = todos()[1];

        var updatedData = [
            {
                label: "Test Todo",
                completed: true,
                id: 1
            },
            {
                label: "Another Todo",
                completed: false,
                id: 3
            }
        ];

        ko.mapping.fromJS(updatedData, new TodoModelMapping(this.server), todos);

        strictEqual(todos().length, 2, "After the update there should still only be 2 items");
        var firstTodo = todos()[0];
        strictEqual(firstTodo, oldFirstTodo, "The first item is still the same object");
        strictEqual(firstTodo.label(), oldFirstTodo.label(), "And it contains the same label");
        strictEqual(firstTodo.id(), oldFirstTodo.id(), "And the same id");
        strictEqual(firstTodo.completed(), true, "But the completed status has changed");

        var secondTodo = todos()[1];
        notStrictEqual(secondTodo, oldSecondTodo, "The second object has changed completely");
        strictEqual(secondTodo.label(), updatedData[1].label, "And contains the specified label");
        strictEqual(secondTodo.id(), updatedData[1].id, "And the specified id");
        strictEqual(secondTodo.completed(), updatedData[1].completed, "And the specified status");
    });
})();