(function() {
    module("General Tests");

    test("Sanity Check", function hello() {
        strictEqual(1 + 1, 2, "Simple sanity check to see if the testing system makes sense");
    });
})();
