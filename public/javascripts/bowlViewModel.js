function BowlViewModel(server) {
    self=this;
    self.volume = ko.observable(1);
    self.rate = ko.observable(2);
    self.currentTemperature = ko.observable(0);
    self.server=server;
    console.log("in bowl view model");

    var data=JSON.stringify({
        "volume": this.volume(),
        "rate" : this.rate()
    })

     self.currentTemperatureCall=function currentTemperatureCall(){
         server.currentTemperatureCall(self.setResponseData,data);
     }
     self.setResponseData = function setResponseData(result){
         console.log("in response ajax handler");
         var finalData=JSON.parse(result.responseText);//data.responseText;
         console.log(finalData.currentTemp);

             //console.log('data:'+data1);
             self.currentTemperature(finalData.currentTemp);// data1.currentTemp;

     }
}
